# Darebee Launcher

Darebee is a free site for fitness. 
It has free workouts, free programs (which bundle those workouts), challenges, mealplans, recipes and more.

This is a small Bash script for automatically launching to the correct day of a [Darebee](https://darebee.com/) [Program](https://darebee.com/programs.html).
This launcher can be used when following a program. By setting the variables, running the script will automatically open up the browser, ready on the right page. This script can for instance be launched on computer boot, to remind the user to do the exercise.




## Usage

Usage is simple: set the correct variables, and launch. An example for the variables is included.

