program = "https://darebee.com/programs/30-days-of-hiit.html"
program_duration = 30 # Amount of days the program lasts
start_day = 1         # Calendar day when started.
start_month = 9       # Calendar month when started.
start_year = 2019     # Calendar year when started.
offset = 1            # Offset, can be used when day was skipped. Default = 1.

##############################################################################
import datetime
import sys
now = datetime.datetime.now()

postamble = "?showall=&start="
start_date = datetime.date(start_year, start_month, start_day)
current_date = datetime.date(now.year, now.month, now.day)

amount_of_days = (current_date - start_date).days
default = "https://darebee.com/programs.html"

if amount_of_days < 0 or program_duration < amount_of_days:
    print("Program ended!")
    sys.exit()

if not 1 <= start_day <= 31 or not 1 <= start_month <= 12:
    print("Invalid date!")
    sys.exit()
    
    
import webbrowser
webbrowser.open("{}{}{}".format(program, postamble, amount_of_days))
