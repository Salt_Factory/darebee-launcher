#!/bin/sh

# Change this to your preferred browser.
export BROWSER=/usr/bin/firefox
export DISPLAY=:1
# Can be run automatically, by for instance adding it to as a cronjob at 16:00.
# Variables to enter:
program="https://darebee.com/programs/core-strength.html" # Link to the program you would like.
program_duration="30" # The amount of days the program lasts. Usually 30.
start_day="4"         # Calendar day when started.
start_month="8"       # Calendar month when started.
start_year="2020"     # Calendar year when started.
offset="1"            # Offset. Can be used when a day was skipped. Default is 1.

##################################################################################################

postamble="?showall=&start="
current_day=`date +"%d"`
current_month=`date +"%m"`
default="https://darebee.com/programs.html"

# Calculate the amount of days in the starting month.
# Generate the calendar, remove all lines with letters, and then count the words.
amount_of_days=`cal $start_month $start_year | egrep -v [a-x] | wc -w`


if [[ 0 -gt $start_day ]] || [[ $start_day -gt $amount_of_days ]] || [[ 0 -gt $start_month ]] || [[ $start_month -gt 12 ]]
then
    echo "Incorrect values"
    exit 1
fi

# If it's the same month, calculate the exercise number and open it in browser.
if [[ $start_month -eq 10#$current_month ]]
then
    exercise=$((10#$current_day - start_day + offset))
    xdg-open $program$postamble$exercise &
else
    echo "different month"
    if [[ $(( 10#$current_month - start_month )) -eq 1 ]]
    then
        # Calculate base difference
        base=$(( amount_of_days - start_day))
        exercise=$(( base + current_day + offset ))
        if [[ $exercise -gt $program_duration ]]
        then
            echo "Program ended! Consider picking a new one."
            exit 1
        fi
        xdg-open $program$postamble$exercise &
    else
        xdg-open $default
    fi
fi
